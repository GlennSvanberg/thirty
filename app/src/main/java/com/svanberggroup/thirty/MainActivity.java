package com.svanberggroup.thirty;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String DIES = "dies";
    private static final String POINTS = "points";
    private static final String ROUNDS = "rounds";
    private static final String TAG = "MainActivity";

    private ImageButton die0, die1, die2, die3, die4, die5;
    private ArrayList<Die> dies;
    private TextView roundTextView;
    private TextView pointsTextView;
    private Button rollButton;
    private int gameRound = 0;
    private int points = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        die0 = findViewById(R.id.die0);
        die1 = findViewById(R.id.die1);
        die2 = findViewById(R.id.die2);
        die3 = findViewById(R.id.die3);
        die4 = findViewById(R.id.die4);
        die5 = findViewById(R.id.die5);
        roundTextView = findViewById(R.id.roundTextView);
        pointsTextView = findViewById(R.id.pointsTextView);
        rollButton = findViewById(R.id.rollButton);

        // Check if there is a saved instance
        if (savedInstanceState != null && savedInstanceState.containsKey(DIES)) {
            dies = savedInstanceState.getParcelableArrayList(DIES);
            points = savedInstanceState.getInt(POINTS);
            gameRound = savedInstanceState.getInt(ROUNDS);
            setText();
            for (Die die : dies) {
                setImage(die);
            }
        } else {
            setupGame();
        }
    }

    public void setupGame() {
        dies = new ArrayList<Die>();
        for (int i = 0; i <= 5; i++) {
            Die die = new Die();
            switch(i) {
                case 0:
                    die.id(die0.getId());
                    break;
                case 1:
                    die.id(die1.getId());
                    break;
                case 2:
                    die.id(die2.getId());
                    break;
                case 3:
                    die.id(die3.getId());
                    break;
                case 4:
                    die.id(die4.getId());
                    break;
                case 5:
                    die.id(die5.getId());
                    break;
            }
            setImage(die);
            dies.add(die);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle state) {
        state.putParcelableArrayList(DIES, dies);
        state.putInt(POINTS, points);
        state.putInt(ROUNDS, gameRound);
        super.onSaveInstanceState(state);
    }

    public void dieClick(View view) {
        for (Die die : dies) {
            if (die.id() == view.getId()) {
                if(!die.isSaved()) {
                    Log.d(TAG, "dieClick: " + die.value());
                    if (die.isActive()) {
                        die.deactivate();
                        points  += die.value();
                    } else {
                        die.activate();
                        points  -= die.value();
                    }
                    setImage(die);
                    setText();
                }
            }
        }
    }

    public void setText() {
        pointsTextView.setText("Points: " + points);
        roundTextView.setText("Round: " + gameRound + "/3");
    }



    public void setImage(Die die) {
        if (die.isSaved()) {
            setRedImage(die);
        }
        else if (die.isActive()) {
            setWhiteImage(die);
        } else {
            setGreyImage(die);
        }
    }

    public void setWhiteImage(Die die) {
        ImageButton button = findViewById(die.id());
        switch(die.value()) {
            case 1:
                button.setImageResource(R.drawable.white1);
                break;
            case 2:
                button.setImageResource(R.drawable.white2);
                break;
            case 3:
                button.setImageResource(R.drawable.white3);
                break;
            case 4:
                button.setImageResource(R.drawable.white4);
                break;
            case 5:
                button.setImageResource(R.drawable.white5);
                break;
            case 6:
                button.setImageResource(R.drawable.white6);
                break;
        }
    }

    public void setGreyImage(Die die) {
        ImageButton button = findViewById(die.id());
        switch(die.value()) {
            case 1:
                button.setImageResource(R.drawable.grey1);
                break;
            case 2:
                button.setImageResource(R.drawable.grey2);
                break;
            case 3:
                button.setImageResource(R.drawable.grey3);
                break;
            case 4:
                button.setImageResource(R.drawable.grey4);
                break;
            case 5:
                button.setImageResource(R.drawable.grey5);
                break;
            case 6:
                button.setImageResource(R.drawable.grey6);
                break;
        }
    }

    public void setRedImage(Die die) {
        ImageButton button = findViewById(die.id());
        switch(die.value()) {
            case 1:
                button.setImageResource(R.drawable.red1);
                break;
            case 2:
                button.setImageResource(R.drawable.red2);
                break;
            case 3:
                button.setImageResource(R.drawable.red3);
                break;
            case 4:
                button.setImageResource(R.drawable.red4);
                break;
            case 5:
                button.setImageResource(R.drawable.red5);
                break;
            case 6:
                button.setImageResource(R.drawable.red6);
                break;
        }
    }

    public void roll(View view) {
        Log.d(TAG, "Rolling dices");

        if(gameRound < 2) {
            for (Die die : dies) {
                if (die.isActive()) {
                    die.roll();
                } else {
                    die.save();
                }
                setImage(die);
            }
            gameRound++;
            setText();
        } else if (gameRound < 3) {
            for (Die die : dies) {
                if (!die.isSaved()) {
                    die.save();
                    points += die.value();
                }
                setRedImage(die);
            }
            gameRound++;
            setText();

            rollButton.setText("SEE RESULT");

            } else {
            Log.d(TAG, "Time for result view");

        }

    }
}
