package com.svanberggroup.thirty;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;


import java.util.Random;

public class Die implements Parcelable {

    private int value;
    private int id;
    private boolean active = true;
    private boolean saved = false;

    public Die() {
        roll();
    }

    protected Die(Parcel in) {
        value = in.readInt();
        id = in.readInt();
        active = in.readByte() != 0;
    }

    public void id(int id) {
        this.id = id;
    }

    public int id() {
        return id;
    }

    public boolean isActive() {
        return active;
    }

    public int value() {
        return value;
    }

    public void deactivate() {
        active = false;
    }

    public void activate() {
        active = true;
    }
    public void save() {
        saved = true;
    }

    public void unsave() {
        saved = false;
    }
    public boolean isSaved() {
        return saved;
    }

    public void roll() {
        Random rand = new Random();
        int i = rand.nextInt(6);
        value = i + 1;
    }
    public static final Creator<Die> CREATOR = new Creator<Die>() {
        @Override
        public Die createFromParcel(Parcel in) {
            return new Die(in);
        }

        @Override
        public Die[] newArray(int size) {
            return new Die[size];
        }
    };
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(value);
        parcel.writeBoolean(active);
        parcel.writeBoolean(saved);
    }
}
